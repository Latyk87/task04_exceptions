package com.epam.controller;
/**
 * Interface for controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public interface Controller {
    /**
     * Method for checked Exception.
     * Created by Borys Latyk on 17/11/2019.
     * @return type of exception.
     */
    String getexceptions() throws Exception;
    /**
     * Method for unchecked Exception.
     * Created by Borys Latyk on 17/11/2019.
     * @return type of exception.
     */
    int getruntime();
    /**
     * Method for try-with-resources.
     * Created by Borys Latyk on 17/11/2019.
     * @return log of recording.
     */
    String getautoclosable() throws Exception;
    /**
     * Method throws a custom Exception in AutoClosable class.
     * Created by Borys Latyk on 17/11/2019.
     * @return custom  exception.
     */
    String getcustom() throws Exception;
}