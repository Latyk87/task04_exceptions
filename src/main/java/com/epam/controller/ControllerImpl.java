package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.Threatment;

/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerImpl implements Controller {

    private Model link;

    public ControllerImpl() {
        link = new Threatment();
    }

    @Override
    public String getexceptions() throws Exception {
        return link.getexceptions();
    }

    @Override
    public int getruntime() {
        return link.getruntime();
    }

    @Override
    public String getautoclosable() throws Exception {
        return link.getautoclosable();
    }

    @Override
    public String getcustom() throws Exception {
        return link.getcustom();
    }
}

