package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;

import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.BufferedReader;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    public View() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Checked exception");
        menu.put("2", "  2 - Unchecked exception");
        menu.put("3", "  3 - AutoClosable class, try-with-resources");
        menu.put("4", "  4 - Throws exception in close() method");
        menu.put("Q", "  Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("Q", this::pressButton5);
    }

    private void pressButton1() {
        try {
            System.out.println(controller.getexceptions());
        } catch (Exception e) {
            System.out.println(e + " File not created");
        }
    }

    private void pressButton2() {
        try {
            System.out.println(controller.getruntime());
        } catch (RuntimeException e) {
            System.out.println(e + " division, programmer check your code");
        }
    }

    private void pressButton3() {


        try {
            System.out.println(controller.getautoclosable());
        } catch (Exception e) {
            System.out.println(e + " not all data was correctly recorded");
        }
    }

    private void pressButton4() {

        try {
            System.out.println(controller.getcustom());
        } catch (Exception e) {
            System.out.println(e + " not all data was correctly recorded");
        }

    }

    private void pressButton5() {
        System.out.println("Bye-Bye");
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nEXCEPTIONS:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}
