package com.epam.model;

import java.io.FileNotFoundException;

/**
 * Class contains example of checked Exception.
 * Created by Borys Latyk on 17/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ThreatOne extends ThreatMain {

    public String generate() throws Exception {
        throw new FileNotFoundException();
    }
}
