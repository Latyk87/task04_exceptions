package com.epam.model;


import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class contains example try-with-resources included and trows a Exception.
 * Created by Borys Latyk on 17/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ThreatThree extends ThreatTwo {
    static int counts = 0;
    FileOutputStream output;

    public ThreatThree(String name) throws IOException {
        super(name);
        output = new FileOutputStream("C:\\exceptions.txt", true);
        output.write(this.getName().getBytes());
        counts++;
    }

    @Override
    public void close() throws Exception {
        if (counts == 6)
            throw new Exception(" LAN connection was lost");
        if (output != null) {
            System.out.println("Data was wrote down into the file correctly");
            System.out.println();
            output.close();
        }


    }
}
