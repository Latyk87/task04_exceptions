package com.epam.model;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class contains example try-with-resources.
 * Created by Borys Latyk on 17/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ThreatTwo extends ThreatOne implements AutoCloseable {
    private String name;
    FileOutputStream output;

    public ThreatTwo(String name) throws IOException {
        this.name = name;
        output = new FileOutputStream("C:\\exceptions.txt", true);
        output.write(this.getName().getBytes());
    }

    public String getName() {
        return name;
    }

    @Override
    public void close() throws Exception {
        if (output != null) {
            System.out.println("Data was wrote down into the file correctly");
            System.out.println();
            output.close();

        }
    }
}