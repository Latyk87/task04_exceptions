package com.epam.model;

/**
 * Class contains example of RuntimeException.
 * Created by Borys Latyk on 17/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ThreatMain extends Exception {

    static int a = 10;
    static int b = 0;
    static int c;


    public int division() {
        c = a / b;
        return c;
    }
}
