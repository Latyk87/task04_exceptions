package com.epam.model;

/**
 * Class generate and control all Threats in following program.
 * Created by Borys Latyk on 17/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class Threatment implements Model {


    @Override
    public String getexceptions() throws Exception {
        ThreatOne e = new ThreatOne();
        e.generate();
        return e.generate();
    }

    @Override
    public int getruntime() {
        ThreatMain a = new ThreatMain();

        return a.division();
    }

    @Override
    public String getautoclosable() throws Exception {
        for (int i = 1; i < 8; i++) {
            String s = "Object" + i + "\r\n";
            try (ThreatTwo epa = new ThreatTwo(s)) {
                System.out.println("Writing the data");
                Thread.sleep(1000);
                System.out.println(epa.getName());
            }

        }
        return "Data was successfully recorded";
    }

    @Override
    public String getcustom() throws Exception {
        for (int i = 1; i < 8; i++) {
            String s = "Object" + i + "\r\n";
            try (ThreatThree epa = new ThreatThree(s)) {
                System.out.println("Writing the data");
                Thread.sleep(1000);
                System.out.println(epa.getName());
            }

        }
        return "Data was successfully recorded";
    }


}
